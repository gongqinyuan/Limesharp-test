<?php

include_once("./limesharptask.php");

$limesharpTask = new LimeSharp();

$task1 = $limesharpTask->repeat([1,2,3]);

//print_r($task1); 
//Reslut
//Array ( [0] => 1 [1] => 2 [2] => 3 [3] => 1 [4] => 2 [5] => 3 [6] => 1 [7] => 2 [8] => 3 )


$task2 = $limesharpTask->reformat("liMeSHArp DeveLoper TEST");

//echo $task2;
//Result
//Lmshrp dvlpr tst

$task3 = $limesharpTask->next_binary_number([1,0,0,0,0,0,0,0,0,1]);

print_r($task3);
//Result (All Tested)
// [1,0] => [1,1]
// [1,1] => [1,0,0]
// [1,1,0] => [1,1,1]
// [1,0,0,0,0,0,0,0,0,1] => [1,0,0,0,0,0,0,0,1,0]
