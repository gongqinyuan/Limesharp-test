<?php

class LimeSharp{

    private $vowels = array('a','e','i','o','u');

    /**
     * task 1
     * 
     */
    public function repeat($arr){

        $newArr = array();

        $newTemp = array_merge($arr,$arr);

        $newArr = array_merge($newTemp,$arr);

        return $newArr;

    }

    /**
     * task 2
     * 
     */
    public function reformat($value){

        $arr = array();
        
        $value = strtolower($value);

        $value = ucfirst($value);


        for ($count = 0; $count < strlen($value); $count++){
            if(!in_array(strtolower($value[$count]),$this->vowels)){
                $arr[] = $value[$count];
            }
        }

        $newValue = join($arr);
        
        return $newValue;
    }

    /**
     * task 3
     * 
     */

    public function next_binary_number($arr){

        $flag = 0;
        $arrayReverse = array_reverse($arr);

        $newArray = array();

        foreach ($arrayReverse as $value){
            if($value == 1 && $flag == 0){
                $newArray[] = 0;
            }elseif($value == 0 && $flag == 0){
                $newArray[] = 1;
                $flag = 1;
            }else{
                $newArray[] = $value;
            }
        }

        if($flag == 0){
            $newArray[] = 1;
        }

        return array_reverse($newArray);

    }


}



